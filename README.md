# dwm-unmanaged

dwm-build with unmanaged script primarily for putting glava in the background. 
Patch source: [dwm-unmanaged](https://github.com/bakkeby/patches/wiki/unmanaged)

## Dependencies:
+ libxft
+ ttf-hack
+ ttf-joypixels
+ st
+ dmenu
+ tabbed
+ Conky for the background scripts shown in the picture
+ glava for the audio visualizer
+ My scripts in my scripts repo are necessary for dwmblocks to work
+ Nerd Fonts (To display all emojis correctly)

![image](dwm-with-conky.png)


To change any keybindings or for an exhaustive list of all of the keybindings, view/edit the ``config.h`` file.
After you make any changes, you must compile dwm again with ``sudo make clean install`` and then restart with
``MODKEY + SHIFT + r`` for changes to take effect.

## Main Keybindings

| Keybinding              | Action                                                       |
|-------------------------|--------------------------------------------------------------|
| MODKEY + RETURN         | opens terminal (I use kitty as my primary terminal)          |
| MODKEY + \ (backspace)  | opens terminal (I use st as my secondary terminal)           |
| MODKEY + SHIFT + RETURN | opens run launcher (dmenu but can be changed)                |
| MODKEY + SHIFT + c      | closes window with focus                                     |
| MODKEY + SHIFT + r      | restarts dwm                                                 |
| MODKEY + SHIFT + -      | quits dwm                                                    |
| MODKEY + b              | hides the bar                                                |
| MODKEY + 1-9            | switch focus to workspace (1-9)                              |
| MODKEY + SHIFT + 1-9    | send focused window to workspace (1-9)                       |
| MODKEY + j              | focus stack +1 (switches focus between windows in the stack) |
| MODKEY + k              | focus stack -1 (switches focus between windows in the stack) |
| MODKEY + SHIFT + j      | rotate stack +1 (rotates the windows in the stack)           |
| MODKEY + SHIFT + k      | rotate stack -1 (rotates the windows in the stack)           |
| MODKEY + h              | setmfact -0.05 (expands size of window)                      |
| MODKEY + l              | setmfact +0.05 (shrinks size of window)                      |
| MODKEY + .              | focusmon +1 (switches focus next monitors)                   |
| MODKEY + ,              | focusmon -1 (switches focus to prev monitors)                |

## Layout controls

| Keybinding             | Action                  |
|------------------------|-------------------------|
| MODKEY + d             | row layout              |
| MODKEY + i             | column layout           |
| MODKEY + TAB           | cycle layout (-1)       |
| MODKEY + SHIFT + TAB   | cycle layout (+1)       |
| MODKEY + SPACE         | change layout           |
| MODKEY + SHIFT + SPACE | toggle floating windows |
| MODKEY + t             | layout 1                |
| MODKEY + f             | layout 2                |
| MODKEY + m             | layout 3                |
| MODKEY + g             | layout 4                |

## Application controls

| Keybinding       | Action                                                                       |
|------------------|------------------------------------------------------------------------------|
| MODKEY + ALT + b | Firefox browser                                                              |
| MODKEY + ALT + c | calculator (requires qalculate-gtk)                                          |
| MODKEY + ALT + d | Discord                                                                      |
| MODKEY + ALT + f | Open File manager (Thunar)                                                   |
| MODKEY + ALT + p | Open power menu (requires my powermenu script from scripts repo)             |
| Print Screen     | Screenshot (requires spectacle)                                              |


